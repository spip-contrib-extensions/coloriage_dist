<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
  // C
  'change_fond'=>'Coloriage du squelette',
  
  // I
  'info' => "Cette page permet de changer les couleurs du squelette par defaut de votre site public (./squelettes-dist/). <p>Cliquer sur dans les carres pour choisir vos nouvelles couleurs</p>
            <p>Pour revenir aux couleurs normales, desactiver le plugin</p>",
    
  
  // E
  'enregistre_couleurs' => 'Enregistrer les couleurs',
   
    // Legende 
    'coloriage_html_bg' => 'Couleur du fond de la page entiere',
    'coloriage_page_bg' => 'Couleur de fond du bloc central',
    'coloriage_a'  => 'Couleur des liens',
    'coloriage_a_hover'  => 'Couleurs des liens (mouseover)',    
    'coloriage_entete_bg' => 'Couleur de fond de l\'entete', 
    'coloriage_contenu_col' => 'Couleur du texte',
    'coloriage_pied_bg' => 'Couleur de fond du pied de page', 
 
);


?>
