<?php

// This is a SPIP language file  --  Questo � un file lingua di SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
  // C
  'change_fond'=>'Colorazione dello squelette',
  
  // I
  'info' => "Questa pagina permette di cambiare i colori dello squelette di defaultt del tuo sito pubblico (./squelettes-dist/). <p>Clicca nei riquadri per scegliere i tuoi nuovi colori</p>
            <p>Per ritornare ai colori normali, disattiva il plugin</p>",
    
  
  // E
  'enregistre_couleurs' => 'Salvare i colori',
   
    // Legende 
    'coloriage_html_bg' => 'Colore dello sfondo dell\'intera pagina',
    'coloriage_page_bg' => 'Colore dello sfondo del blocco centrale',
    'coloriage_a'  => 'Colore dei link',
    'coloriage_a_hover'  => 'Colori dei link (mouseover)',    
    'coloriage_entete_bg' => 'Colore dello sfondo dell\'intestazione', 
    'coloriage_contenu_col' => 'Colore del testo',
    'coloriage_pied_bg' => 'Colore dello sfondo del pi&egrave; di pagina', 
 
);


?>
